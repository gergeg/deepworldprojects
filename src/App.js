import React from 'react';
import ReactDOM from 'react-dom';

import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';

import { makeStyles } from '@material-ui/core/styles';
import { sizing } from '@material-ui/system';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';


import './index.css';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 5,
  },
  button: {
    margin: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width: '70px',
    height:'70px',
    fontSize: 40,
  },
  input: {
    display: 'none',
  },
}));



function Square(props) {
  const classes = useStyles();

  return (
    <Button variant="contained" className={classes.button} onClick={props.onClick} fullWidth>{props.value ? props.value : ' '}</Button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return <Square value={this.props.squares[i]} onClick={() => this.props.onClick(i)}/>;
  }

  render() {
    return (
        <Grid container spacing={1} className={useStyles.root}>
          <Grid item xs={4} alignContent={'center'} alignItems={'center'} justify={'center'}>
            {this.renderSquare(0)}
          </Grid>
          <Grid item xs={4} zeroMinWidth>
            {this.renderSquare(1)}
          </Grid>
          <Grid item xs={4} zeroMinWidth>
            {this.renderSquare(2)}
          </Grid>
          <Grid item xs={4}>
            {this.renderSquare(3)}
          </Grid>
          <Grid item xs={4}>
            {this.renderSquare(4)}
          </Grid>
          <Grid item xs={4}>
            {this.renderSquare(5)}
          </Grid>
          <Grid item xs={4}>
            {this.renderSquare(6)}
          </Grid>
          <Grid item xs={4}>
            {this.renderSquare(7)}
          </Grid>
          <Grid item xs={4}>
            {this.renderSquare(8)}
          </Grid>
        </Grid>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
   super(props);
   this.state = {
     history: [{
       squares: Array(9).fill(null),
     }],
     stepNumber: 0,
     xIsNext: true,
   };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length-1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares,
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  jumpTo(step) {
    this.setState({
        stepNumber: step,
        xIsNext: (step % 2) === 0,
    });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner  = calculateWinner(current.squares);

    const moves = history.map((step,move) => {
      const desc = move ? 'Go to move #' + move : 'Go to game start';
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;
     if (winner) {
       status = 'Winner: ' + winner;
     } else {
       status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
     }

    return (
      <Board squares={current.squares} onClick={(i) => this.handleClick(i)} />
    );
  }
}


function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

export default function App() {
  return (
    <Container maxWidth="sm">
      <Game />
    </Container>
  );
}
